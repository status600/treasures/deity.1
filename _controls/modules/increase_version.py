

import rich
import tomllib
import semver
import toml

def increase_version (
	bump = "patch"
):
	pyproject_path = "pyproject.toml"

	with open (pyproject_path, "rb") as f:
		data = tomllib.load (f)
					
	version = semver.Version.parse (data ["project"] ["version"])
	
	if (bump == "patch"):
		version = version.bump_patch ()
	else:
		raise Exception ("")
		
	data ["project"] ["version"] = str (version)

	rich.print_json (data = data)

	write_pointer = open (pyproject_path, 'w')
	toml.dump (data, write_pointer)
	write_pointer.close ()
	