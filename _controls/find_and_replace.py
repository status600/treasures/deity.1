
def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'modules_pip'
])


import ships.paths.directory.find_and_replace_string_v2 as find_and_replace_string_v2

import pathlib
from os.path import dirname, join, normpath
this_directory = pathlib.Path (__file__).parent.resolve ()


places = [
	str (normpath (join (this_directory, "../venues/stages"))),
	str (normpath (join (this_directory, "../venues/warehouse"))),

	str (normpath (join (this_directory, "../venues_pypi"))),


	#
	#	
	#
	str (normpath (join (this_directory, "../docker-compose.yml"))),
	str (normpath (join (this_directory, "../install.sh"))),
	str (normpath (join (this_directory, "../license.S.HTML"))),
	str (normpath (join (this_directory, "../pyproject.toml"))),
	str (normpath (join (this_directory, "../readme.md"))),
	str (normpath (join (this_directory, "../sources.sh"))),
	str (normpath (join (this_directory, "../venue.O.HTML")))
]

for place in places:
	print ("place:", place)

	find_and_replace_string_v2.start (
		the_path = place,

		find = 'emergency',
		replace_with = 'Emergency',
		
		replace_contents = "yes",
		replace_paths = "yes"
	)