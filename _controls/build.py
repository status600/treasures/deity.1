




'''
	https://github.com/astral-sh/uv
'''

import os

def run (strand):
	os.system (strand)


run ("apt install unzip; curl -fsSL https://bun.sh/install | bash; . /root/.bashrc")

#
#	uv
#		curl -LsSf https://astral.sh/uv/install.sh | sh
#		source $HOME/.cargo/env
#
#			# or pip install uv
#	
#	uv venv:
#		uv venv
#		source .venv/bin/activate
#		uv pip sync requirements.txt 
#
#	system:
#		UV_SYSTEM_PYTHON=/biotech/venues/stages_pip2 uv pip sync requirements.txt --system
#
def build_with_uv ():
	def generate_uv_requirements ():
		run (f"cd /habitat && uv pip compile pyproject.toml -o requirements.txt")

	def build_uv_venv ():
		run (f"cd /habitat && uv venv")
		run (f"cd /habitat && . .venv/bin/activate")
		run (f"cd /habitat && uv pip sync requirements.txt")

	generate_uv_requirements ()
	build_uv_venv ()

run ("pip install uv")

build_with_uv ()

